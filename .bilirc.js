/** @type {import('bili').Config} */
module.exports = {
  input: 'src/index.js',
  output: {
    dir: 'dist',
    format: ['cjs']
  }
}
