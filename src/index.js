import React from 'react'

const moonCake = require('../assets/mooncake.png')

function ReactComponent({ caption = 'Mooncake' }) {
  return (
    <figure>
      <img src={moonCake} alt={caption} />
      <figcaption aria-hidden="true">{caption}</figcaption>
    </figure>
  )
}

export default ReactComponent
